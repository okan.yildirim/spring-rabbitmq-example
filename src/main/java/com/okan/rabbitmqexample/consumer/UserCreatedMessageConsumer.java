package com.okan.rabbitmqexample.consumer;

import com.okan.rabbitmqexample.model.UserMessage;
import com.okan.rabbitmqexample.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class UserCreatedMessageConsumer {

    private final Logger logger = LoggerFactory.getLogger(UserCreatedMessageConsumer.class);
    private final EmailService emailService;

    public UserCreatedMessageConsumer(EmailService emailService) {
        this.emailService = emailService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.user-registered.queue}",
            autoStartup = "${spring.rabbitmq.user-registered.enabled}")
    public void listen(UserMessage userMessage) {
        try {
            logger.info("User message consumed, {}", userMessage);
            emailService.sendUserRegisteredMail(userMessage);
        } catch (Exception e) {
            logger.error("Exception occurred when consuming user message {} ,", userMessage, e);
            throw e;
        }
    }
}
