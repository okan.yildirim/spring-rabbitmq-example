package com.okan.rabbitmqexample.consumer;

import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import com.okan.rabbitmqexample.service.StockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class OrderCreatedMessageConsumerForStock {
    private final Logger logger = LoggerFactory.getLogger(OrderCreatedMessageConsumerForStock.class);
    private final StockService stockService;

    public OrderCreatedMessageConsumerForStock(StockService stockService) {
        this.stockService = stockService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.order-created.stock.queue}",
            autoStartup = "${spring.rabbitmq.order-created.stock.enabled}")
    public void listen(OrderCreatedMessage orderCreatedMessage) {
        try {
            logger.info("Order created message consumed, {}", orderCreatedMessage);
            stockService.updateStock(orderCreatedMessage);
        } catch (Exception e) {
            logger.error("Exception occurred when consuming order created message {} ,", orderCreatedMessage, e);
            throw e;
        }
    }
}
