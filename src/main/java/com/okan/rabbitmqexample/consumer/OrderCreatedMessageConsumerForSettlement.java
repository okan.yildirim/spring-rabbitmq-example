package com.okan.rabbitmqexample.consumer;

import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import com.okan.rabbitmqexample.service.SettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class OrderCreatedMessageConsumerForSettlement {
    private final Logger logger = LoggerFactory.getLogger(OrderCreatedMessageConsumerForSettlement.class);
    private final SettlementService settlementService;

    public OrderCreatedMessageConsumerForSettlement(SettlementService settlementService) {
        this.settlementService = settlementService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.order-created.settlement.queue}",
            autoStartup = "${spring.rabbitmq.order-created.settlement.enabled}")
    public void listen(OrderCreatedMessage orderCreatedMessage) {
        try {
            logger.info("Order created message consumed, {}", orderCreatedMessage);
            settlementService.saveSettlement(orderCreatedMessage);
        } catch (Exception e) {
            logger.error("Exception occurred when consuming order created message {} ,", orderCreatedMessage, e);
            throw e;
        }
    }
}
