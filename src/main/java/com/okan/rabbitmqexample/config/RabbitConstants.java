package com.okan.rabbitmqexample.config;

public class RabbitConstants {

    public static final String X_DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";
    public static final String X_DEAD_LETTER_ROUTING_KEY = "x-dead-letter-routing-key";
    public static final String DEAD_LETTER = "deadLetter";
}
