package com.okan.rabbitmqexample.config.direct;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.okan.rabbitmqexample.config.RabbitConstants.DEAD_LETTER;
import static com.okan.rabbitmqexample.config.RabbitConstants.X_DEAD_LETTER_EXCHANGE;
import static com.okan.rabbitmqexample.config.RabbitConstants.X_DEAD_LETTER_ROUTING_KEY;

@Configuration
public class UserRegisteredRabbitConfig {

    private final UserRegisteredRabbitProperties userRegisteredRabbitProperties;

    public UserRegisteredRabbitConfig(UserRegisteredRabbitProperties userRegisteredRabbitProperties) {
        this.userRegisteredRabbitProperties = userRegisteredRabbitProperties;
    }

    @Bean
    Queue queue() {
        return QueueBuilder.durable(userRegisteredRabbitProperties.getQueue())
                .withArgument(X_DEAD_LETTER_EXCHANGE, userRegisteredRabbitProperties.getDeadLetter())
                .withArgument(X_DEAD_LETTER_ROUTING_KEY, DEAD_LETTER)
                .build();
    }

    @Bean
    Queue deadLetterQueue() {
        return QueueBuilder.durable(userRegisteredRabbitProperties.getDeadLetter()).build();
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(userRegisteredRabbitProperties.getExchange());
    }

    @Bean
    DirectExchange deadLetterExchange() {
        return new DirectExchange(userRegisteredRabbitProperties.getDeadLetter());
    }

    @Bean
    Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with(userRegisteredRabbitProperties.getRoutingKey());
    }

    @Bean
    Binding deadLetterBinding() {
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with(DEAD_LETTER);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
