package com.okan.rabbitmqexample.config.fanout;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq.order-created.settlement")
public class OrderCreatedSettlementRabbitProperties {

    @Value("${spring.rabbitmq.order-created.exchange}")
    private String exchange;

    private String queue;
    private String deadLetter;
    private boolean enabled;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getDeadLetter() {
        return deadLetter;
    }

    public void setDeadLetter(String deadLetter) {
        this.deadLetter = deadLetter;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }
}
