package com.okan.rabbitmqexample.config.fanout;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.okan.rabbitmqexample.config.RabbitConstants.DEAD_LETTER;
import static com.okan.rabbitmqexample.config.RabbitConstants.X_DEAD_LETTER_EXCHANGE;
import static com.okan.rabbitmqexample.config.RabbitConstants.X_DEAD_LETTER_ROUTING_KEY;

@Configuration
public class OrderCreatedRabbitConfig {

    private final OrderCreatedStockRabbitProperties stockRabbitProperties;
    private final OrderCreatedSettlementRabbitProperties settlementRabbitProperties;

    public OrderCreatedRabbitConfig(OrderCreatedStockRabbitProperties stockRabbitProperties, OrderCreatedSettlementRabbitProperties settlementRabbitProperties) {
        this.stockRabbitProperties = stockRabbitProperties;
        this.settlementRabbitProperties = settlementRabbitProperties;
    }

    @Bean
    Queue stockQueue() {
        return QueueBuilder.durable(stockRabbitProperties.getQueue())
                .withArgument(X_DEAD_LETTER_EXCHANGE, stockRabbitProperties.getDeadLetter())
                .withArgument(X_DEAD_LETTER_ROUTING_KEY, DEAD_LETTER)
                .build();
    }

    @Bean
    Queue settlementQueue() {
        return QueueBuilder.durable(settlementRabbitProperties.getQueue())
                .withArgument(X_DEAD_LETTER_EXCHANGE, settlementRabbitProperties.getDeadLetter())
                .withArgument(X_DEAD_LETTER_ROUTING_KEY, DEAD_LETTER)
                .build();
    }

    @Bean
    Queue stockDeadLetterQueue() {
        return QueueBuilder.durable(stockRabbitProperties.getDeadLetter()).build();
    }

    @Bean
    Queue settlementDeadLetterQueue() {
        return QueueBuilder.durable(settlementRabbitProperties.getDeadLetter()).build();
    }

    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange(settlementRabbitProperties.getExchange());
    }

    @Bean
    DirectExchange stockDeadLetterExchange() {
        return new DirectExchange(stockRabbitProperties.getDeadLetter());
    }

    @Bean
    DirectExchange settlementDeadLetterExchange() {
        return new DirectExchange(settlementRabbitProperties.getDeadLetter());
    }

    @Bean
    Binding stockBinding() {
        return BindingBuilder.bind(stockQueue()).to(fanoutExchange());
    }

    @Bean
    Binding settlementBinding() {
        return BindingBuilder.bind(settlementQueue()).to(fanoutExchange());
    }

    @Bean
    Binding stockDeadLetterBinding() {
        return BindingBuilder.bind(stockDeadLetterQueue()).to(stockDeadLetterExchange()).with(DEAD_LETTER);
    }

    @Bean
    Binding settlementDeadLetterBinding() {
        return BindingBuilder.bind(settlementDeadLetterQueue()).to(settlementDeadLetterExchange()).with(DEAD_LETTER);
    }

    @Bean("orderMessageConverter")
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
