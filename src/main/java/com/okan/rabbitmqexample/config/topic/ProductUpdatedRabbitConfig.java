package com.okan.rabbitmqexample.config.topic;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductUpdatedRabbitConfig {

    private final ProductDeletedRabbitProperties deletedRabbitProperties;
    private final ProductCreatedRabbitProperties createdRabbitProperties;
    private final ProductUpdatedRabbitProperties updatedRabbitProperties;

    public ProductUpdatedRabbitConfig(ProductDeletedRabbitProperties deletedRabbitProperties,
                                      ProductCreatedRabbitProperties createdRabbitProperties,
                                      ProductUpdatedRabbitProperties updatedRabbitProperties) {
        this.deletedRabbitProperties = deletedRabbitProperties;
        this.createdRabbitProperties = createdRabbitProperties;
        this.updatedRabbitProperties = updatedRabbitProperties;
    }

    @Bean
    Queue createdQueue() {
        return QueueBuilder.durable(createdRabbitProperties.getQueue()).build();
    }

    @Bean
    Queue deletedQueue() {
        return QueueBuilder.durable(deletedRabbitProperties.getQueue()).build();
    }

    @Bean
    Queue updatedQueue() {
        return QueueBuilder.durable(updatedRabbitProperties.getQueue()).build();
    }

    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange(createdRabbitProperties.getExchange());
    }

    @Bean
    Binding createBinding() {
        return BindingBuilder.bind(createdQueue()).to(topicExchange()).with(createdRabbitProperties.getRoutingKey());
    }

    @Bean
    Binding deletedBinding() {
        return BindingBuilder.bind(deletedQueue()).to(topicExchange()).with(deletedRabbitProperties.getRoutingKey());
    }

    @Bean
    Binding updateBinding() {
        return BindingBuilder.bind(updatedQueue()).to(topicExchange()).with(updatedRabbitProperties.getRoutingKey());
    }

    @Bean("productMessageConverter")
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
