package com.okan.rabbitmqexample.producer;

import com.okan.rabbitmqexample.config.direct.UserRegisteredRabbitProperties;
import com.okan.rabbitmqexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class UserCreatedMessageProducer {

    private final Logger logger = LoggerFactory.getLogger(UserCreatedMessageProducer.class);

    private final RabbitTemplate rabbitTemplate;
    private final UserRegisteredRabbitProperties rabbitProperties;

    public UserCreatedMessageProducer(RabbitTemplate rabbitTemplate, UserRegisteredRabbitProperties rabbitProperties) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitProperties = rabbitProperties;
    }

    public void send(UserMessage userMessage) {
        rabbitTemplate.convertAndSend(rabbitProperties.getExchange(), rabbitProperties.getRoutingKey(), userMessage);

        logger.info("UserMessage send to exchange: {} , routingKey: {} , message: {} ",
                rabbitProperties.getExchange(), rabbitProperties.getRoutingKey(), userMessage);
    }
}
