package com.okan.rabbitmqexample.producer;

import com.okan.rabbitmqexample.config.direct.UserRegisteredRabbitProperties;
import com.okan.rabbitmqexample.config.topic.ProductCreatedRabbitProperties;
import com.okan.rabbitmqexample.config.topic.ProductDeletedRabbitProperties;
import com.okan.rabbitmqexample.config.topic.ProductUpdatedRabbitProperties;
import com.okan.rabbitmqexample.model.ProductUpdatedMessage;
import com.okan.rabbitmqexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProductUpdateMessageProducer {

    private final Logger logger = LoggerFactory.getLogger(ProductUpdateMessageProducer.class);

    private final RabbitTemplate rabbitTemplate;
    private final ProductDeletedRabbitProperties deletedRabbitProperties;
    private final ProductCreatedRabbitProperties createdRabbitProperties;
    private final ProductUpdatedRabbitProperties updatedRabbitProperties;

    public ProductUpdateMessageProducer(RabbitTemplate rabbitTemplate,
                                        ProductDeletedRabbitProperties deletedRabbitProperties,
                                        ProductCreatedRabbitProperties createdRabbitProperties,
                                        ProductUpdatedRabbitProperties updatedRabbitProperties) {
        this.rabbitTemplate = rabbitTemplate;
        this.deletedRabbitProperties = deletedRabbitProperties;
        this.createdRabbitProperties = createdRabbitProperties;
        this.updatedRabbitProperties = updatedRabbitProperties;
    }

    public void sendCreated(ProductUpdatedMessage productUpdatedMessage) {
        rabbitTemplate.convertAndSend(createdRabbitProperties.getExchange(), createdRabbitProperties.getRoutingKey(), productUpdatedMessage);

        logger.info("ProductUpdatedMessage send to exchange: {} , routingKey: {} , message: {} ",
                createdRabbitProperties.getExchange(), createdRabbitProperties.getRoutingKey(), productUpdatedMessage);
    }

    public void sendDeleted(ProductUpdatedMessage productUpdatedMessage) {
        rabbitTemplate.convertAndSend(deletedRabbitProperties.getExchange(), deletedRabbitProperties.getRoutingKey(), productUpdatedMessage);

        logger.info("ProductUpdatedMessage send to exchange: {} , routingKey: {} , message: {} ",
                deletedRabbitProperties.getExchange(), deletedRabbitProperties.getRoutingKey(), productUpdatedMessage);
    }

    public void sendUpdated(ProductUpdatedMessage productUpdatedMessage) {
        rabbitTemplate.convertAndSend(updatedRabbitProperties.getExchange(), updatedRabbitProperties.getRoutingKey(), productUpdatedMessage);

        logger.info("ProductUpdatedMessage send to exchange: {} , routingKey: {} , message: {} ",
                updatedRabbitProperties.getExchange(), updatedRabbitProperties.getRoutingKey(), productUpdatedMessage);
    }
}
