package com.okan.rabbitmqexample.producer;

import com.okan.rabbitmqexample.config.fanout.OrderCreatedStockRabbitProperties;
import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderCreatedMessageProducer {

    private final Logger logger = LoggerFactory.getLogger(OrderCreatedMessageProducer.class);

    private final RabbitTemplate rabbitTemplate;
    private final OrderCreatedStockRabbitProperties rabbitProperties; // it can be settlement properties

    public OrderCreatedMessageProducer(RabbitTemplate rabbitTemplate, OrderCreatedStockRabbitProperties rabbitProperties) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitProperties = rabbitProperties;
    }


    public void send(OrderCreatedMessage orderMessage) {
        rabbitTemplate.convertAndSend(rabbitProperties.getExchange(), "", orderMessage);

        logger.info("UserMessage send to exchange: {} , routingKey: {} , message: {} ",
                rabbitProperties.getExchange(), "", orderMessage);
    }
}
