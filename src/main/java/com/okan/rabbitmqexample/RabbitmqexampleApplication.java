package com.okan.rabbitmqexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RabbitmqexampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqexampleApplication.class, args);
    }

}
