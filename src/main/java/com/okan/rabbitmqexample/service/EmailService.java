package com.okan.rabbitmqexample.service;

import com.okan.rabbitmqexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    private final Logger logger = LoggerFactory.getLogger(EmailService.class);

    public void sendUserRegisteredMail(UserMessage userMessage) {

        if (userMessage.getAge() > 60) {
            throw new IllegalStateException("Age must be bigger than 50");
        }
        logger.info("Email is send for: {}", userMessage);
    }
}
