package com.okan.rabbitmqexample.service;

import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SettlementService {

    private final Logger logger = LoggerFactory.getLogger(SettlementService.class);

    public void saveSettlement(OrderCreatedMessage orderCreatedMessage) {

        if (orderCreatedMessage.getPrice() > 900) {
            throw new IllegalStateException("Price must be small than 900");
        }
        logger.info("Settlement saved for: {}", orderCreatedMessage);
    }
}
