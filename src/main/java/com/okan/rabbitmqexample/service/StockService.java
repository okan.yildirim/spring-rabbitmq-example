package com.okan.rabbitmqexample.service;

import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class StockService {

    private final Logger logger = LoggerFactory.getLogger(StockService.class);

    public void updateStock(OrderCreatedMessage orderCreatedMessage) {

        if (orderCreatedMessage.getPrice() > 800) {
            throw new IllegalStateException("Price must be small than 800");
        }
        logger.info("Stock updated for: {}", orderCreatedMessage);
    }
}
