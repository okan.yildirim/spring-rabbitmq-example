package com.okan.rabbitmqexample.model;

public enum ChangeType {
    CREATED, DELETED, UPDATED
}
