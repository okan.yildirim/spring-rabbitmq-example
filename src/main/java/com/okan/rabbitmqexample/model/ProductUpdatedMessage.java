package com.okan.rabbitmqexample.model;

import java.io.Serializable;

public class ProductUpdatedMessage implements Serializable {

    private Long id;
    private String name;
    private Double price;
    private ChangeType changeType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ChangeType getChangeType() {
        return changeType;
    }

    public void setChangeType(ChangeType changeType) {
        this.changeType = changeType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductUpdatedMessage{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append(", changeType=").append(changeType);
        sb.append('}');
        return sb.toString();
    }
}
