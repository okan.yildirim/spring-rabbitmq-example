package com.okan.rabbitmqexample.job;

import com.github.javafaker.Faker;
import com.okan.rabbitmqexample.model.ChangeType;
import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import com.okan.rabbitmqexample.model.ProductUpdatedMessage;
import com.okan.rabbitmqexample.model.UserMessage;
import com.okan.rabbitmqexample.producer.OrderCreatedMessageProducer;
import com.okan.rabbitmqexample.producer.ProductUpdateMessageProducer;
import com.okan.rabbitmqexample.producer.UserCreatedMessageProducer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DummyMessageCreateJob {

    private final UserCreatedMessageProducer userCreatedMessageProducer;
    private final OrderCreatedMessageProducer orderCreatedMessageProducer;
    private final ProductUpdateMessageProducer productUpdateMessageProducer;

    public DummyMessageCreateJob(UserCreatedMessageProducer userCreatedMessageProducer,
                                 OrderCreatedMessageProducer orderCreatedMessageProducer,
                                 ProductUpdateMessageProducer productUpdateMessageProducer) {
        this.userCreatedMessageProducer = userCreatedMessageProducer;
        this.orderCreatedMessageProducer = orderCreatedMessageProducer;
        this.productUpdateMessageProducer = productUpdateMessageProducer;
    }

    @Scheduled(fixedRate = 1000)
    public void generateUserAndSend() {
        UserMessage dummyUserMessage = getDummyUserMessage();
        userCreatedMessageProducer.send(dummyUserMessage);
    }

    @Scheduled(fixedRate = 1000)
    public void generateOrderAndSend() {
        OrderCreatedMessage dummyOrderMessage = getDummyOrderMessage();
        orderCreatedMessageProducer.send(dummyOrderMessage);
    }

    @Scheduled(fixedRate = 1000)
    public void generateProductAndSend() {
        ProductUpdatedMessage dummyProductUpdateMessage = getDummyProductUpdateMessage();

        if (dummyProductUpdateMessage.getPrice() < 700D) {
            dummyProductUpdateMessage.setChangeType(ChangeType.CREATED);
            productUpdateMessageProducer.sendCreated(dummyProductUpdateMessage);

        } else if (dummyProductUpdateMessage.getPrice() > 700D && dummyProductUpdateMessage.getPrice() < 900D) {
            dummyProductUpdateMessage.setChangeType(ChangeType.UPDATED);
            productUpdateMessageProducer.sendUpdated(dummyProductUpdateMessage);
        } else {
            dummyProductUpdateMessage.setChangeType(ChangeType.DELETED);
            productUpdateMessageProducer.sendDeleted(dummyProductUpdateMessage);
        }
    }

    private UserMessage getDummyUserMessage() {
        Faker faker = new Faker();
        String firstName = faker.name().firstName();
        String surname = faker.name().lastName();
        String email = firstName.toLowerCase() + "." + surname.toLowerCase() + "@email.com";

        UserMessage userMessage = new UserMessage();
        userMessage.setId(faker.number().numberBetween(0L, 999999L));
        userMessage.setName(firstName);
        userMessage.setSurname(surname);
        userMessage.setEmail(email);
        userMessage.setAge(faker.number().numberBetween(18, 90));
        return userMessage;
    }

    private OrderCreatedMessage getDummyOrderMessage() {
        Faker faker = new Faker();

        OrderCreatedMessage orderCreatedMessage = new OrderCreatedMessage();
        orderCreatedMessage.setId(faker.number().numberBetween(0L, 999999L));
        orderCreatedMessage.setName(faker.food().dish());
        orderCreatedMessage.setPrice(faker.number().randomDouble(2,1,1000));

        return orderCreatedMessage;
    }

    private ProductUpdatedMessage getDummyProductUpdateMessage() {
        Faker faker = new Faker();

        ProductUpdatedMessage productUpdateMessage = new ProductUpdatedMessage();
        productUpdateMessage.setId(faker.number().numberBetween(0L, 999999L));
        productUpdateMessage.setName(faker.food().dish());
        productUpdateMessage.setPrice(faker.number().randomDouble(2, 1, 1000));

        return productUpdateMessage;
    }

}
