package com.okan.rabbitmqexample.consumer;

import com.okan.rabbitmqexample.AbstractRabbitContainer;
import com.okan.rabbitmqexample.config.direct.UserRegisteredRabbitConfig;
import com.okan.rabbitmqexample.config.direct.UserRegisteredRabbitProperties;
import com.okan.rabbitmqexample.model.UserMessage;
import com.okan.rabbitmqexample.service.EmailService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {UserCreatedMessageConsumer.class,
        UserRegisteredRabbitConfig.class})
@EnableConfigurationProperties(UserRegisteredRabbitProperties.class)
@ImportAutoConfiguration(RabbitAutoConfiguration.class)
class UserCreatedMessageConsumerTest extends AbstractRabbitContainer {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    UserRegisteredRabbitProperties rabbitProperties;

    @MockBean
    EmailService emailService;

    @Test
    public void it_should_consume_user_registered_message() {
        //given
        UserMessage userMessage = new UserMessage();
        userMessage.setAge(24);
        userMessage.setId(123L);
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        //when
        rabbitTemplate.convertAndSend(rabbitProperties.getExchange(), rabbitProperties.getRoutingKey(), userMessage);
        //then

        ArgumentCaptor<UserMessage> userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);
        verify(emailService,  timeout(3000)).sendUserRegisteredMail(userMessageArgumentCaptor.capture());
        UserMessage captorValue = userMessageArgumentCaptor.getValue();

        assertThat(captorValue).isEqualToComparingFieldByField(userMessage);
    }
}