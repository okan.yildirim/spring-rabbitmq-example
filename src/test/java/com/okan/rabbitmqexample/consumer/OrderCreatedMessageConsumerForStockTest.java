package com.okan.rabbitmqexample.consumer;

import com.okan.rabbitmqexample.AbstractRabbitContainer;
import com.okan.rabbitmqexample.config.fanout.OrderCreatedRabbitConfig;
import com.okan.rabbitmqexample.config.fanout.OrderCreatedSettlementRabbitProperties;
import com.okan.rabbitmqexample.config.fanout.OrderCreatedStockRabbitProperties;
import com.okan.rabbitmqexample.model.OrderCreatedMessage;
import com.okan.rabbitmqexample.service.StockService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {OrderCreatedMessageConsumerForStock.class,
        OrderCreatedRabbitConfig.class})
@EnableConfigurationProperties({OrderCreatedStockRabbitProperties.class,
        OrderCreatedSettlementRabbitProperties.class})
@ImportAutoConfiguration(RabbitAutoConfiguration.class)
class OrderCreatedMessageConsumerForStockTest extends AbstractRabbitContainer {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @MockBean
    StockService stockService;

    @Autowired
    OrderCreatedStockRabbitProperties rabbitProperties;

    @Test
    public void it_should_consume_order_created_service() {
        //given

        OrderCreatedMessage orderCreatedMessage = new OrderCreatedMessage();
        orderCreatedMessage.setName("name");
        orderCreatedMessage.setId(1L);
        orderCreatedMessage.setPrice(20D);
        //when

        rabbitTemplate.convertAndSend(rabbitProperties.getExchange(), "", orderCreatedMessage);
        //then

        ArgumentCaptor<OrderCreatedMessage> orderCreatedMessageArgumentCaptor = ArgumentCaptor.forClass(OrderCreatedMessage.class);
        verify(stockService, timeout(3000)).updateStock(orderCreatedMessageArgumentCaptor.capture());
        OrderCreatedMessage capturedValue = orderCreatedMessageArgumentCaptor.getValue();

        assertThat(capturedValue).isEqualToComparingFieldByField(orderCreatedMessage);
    }
}