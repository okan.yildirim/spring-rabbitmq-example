package com.okan.rabbitmqexample;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.containers.wait.strategy.Wait;

@ContextConfiguration(initializers = AbstractRabbitContainer.Initializer.class)
public abstract class AbstractRabbitContainer {

    protected static RabbitMQContainer rabbitMqContainer;

    static {
        rabbitMqContainer = new RabbitMQContainer("rabbitmq:3.8-alpine")
                .withExposedPorts(5672,15672)
                .waitingFor(Wait.forLogMessage(".*Server startup complete.*\n", 1));
        rabbitMqContainer.start();
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

            TestPropertyValues.of(
                    "spring.rabbitmq.host=" + rabbitMqContainer.getContainerIpAddress(),
                    "spring.rabbitmq.port=" + rabbitMqContainer.getMappedPort(5672),
                    "spring.rabbitmq.username=" + rabbitMqContainer.getAdminUsername(),
                    "spring.rabbitmq.password=" + rabbitMqContainer.getAdminPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
